%module(directors="1") pyTSF

%{
#include "json.hpp"
#include "core/misc.h"
#include "core/vector.hh"
#include "core/game_state.h"
#include "core/listeners/game_listener.h"
#include "core/clock.h"
#include "core/collision_checking.h"
#include "core/game.h"
#include "core/logger.h"
#include "core/objects/abstract_game_object.h"
#include "core/objects/activation_region.h"
#include "core/objects/border.h"
#include "core/objects/fortress.h"
#include "core/objects/missile.h"
#include "core/objects/player.h"
#include "core/objects/shell.h"
#include "core/objects/shield.h"
#include "core/listeners/fortress_listener.h"
#include "core/listeners/missile_listener.h"
#include "core/listeners/player_listener.h"
#include "core/listeners/ui_listener.h"
#include "builder/json_game_builder.h"
#include "agents/async_agent.h"
%}

%include "typemaps.i"
%include "std_vector.i"
%include "std_string.i"

%feature("director") AbstractClockListener;
%feature("director") AsynchronousAgent;
%feature("director") CallbackAgent;
%feature("director") GameListener;
%feature("director") FortressListener;
%feature("director") MissileListener;
%feature("director") PlayerListener;
%feature("director") UIListener;
%feature("notabstract") AsynchronousAgent;
%feature("notabstract") CallbackAgent;

//%include "json.hpp"
%include "core/misc.h"
%include "core/vector.hh"
%include "core/game_state.h"
%include "core/listeners/game_listener.h"
%include "core/clock.h"
%include "core/collision_checking.h"
%include "core/game.h"
%include "core/logger.h"
%include "core/objects/abstract_game_object.h"
%include "core/objects/activation_region.h"
%include "core/objects/border.h"
%include "core/objects/fortress.h"
%include "core/objects/missile.h"
%include "core/objects/player.h"
%include "core/objects/shell.h"
%include "core/objects/shield.h"
%include "core/listeners/fortress_listener.h"
%include "core/listeners/missile_listener.h"
%include "core/listeners/player_listener.h"
%include "core/listeners/ui_listener.h"
%include "builder/json_game_builder.h"
%include "agents/async_agent.h"

namespace std
{
	%template(PlayerStateVector) vector<PlayerState>;
	%template(FortressStateVector) vector<FortressState>;
	%template(MissileStateVector) vector<MissileState>;
	%template(ShellStateVector) vector<ShellState>;

	%template(KeyVector) vector<Key>;

	%template(VectorPointerVector) vector<Vector*>;
	%template(StringVector) vector<string>;
}
